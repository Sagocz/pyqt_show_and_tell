#!/usr/bin/env python
from subscriber import CtrlCmdReceiver
from param_adjuster import Ui_MainWindow
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QObject
from datetime import datetime


class Controller(Ui_MainWindow, QObject):
    # Setup elements on GUI

    def setupUi(self, MainWindow):
        # Calling generated class setupUi
        Ui_MainWindow.setupUi(self, MainWindow)

        self.param_node = CtrlCmdReceiver()
        self.stanley_ke_spin_box.valueChanged.connect(self.set_params)

    @QtCore.pyqtSlot()
    def set_params(self):
        self.param_node.set_stanley_ke(self.stanley_ke_spin_box.value())
        self.param_node.new_timer_callback()

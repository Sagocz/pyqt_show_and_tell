#!/usr/bin/env python
import rclpy
from rclpy.node import Node
from PyQt5.QtCore import QObject, pyqtSignal
from threading import Thread


class CtrlCmdReceiver(Node, QObject):
    widget_signal = pyqtSignal()

    def __init__(self):
        super(CtrlCmdReceiver, self).__init__("param_adjuster")
        QObject.__init__(self)
        # self.__ctrl_cmd_msg_values = RawControlCommandOneAxis()
        # self.node = rclpy.create_node("param_adjuster")
        # self.node.declare_parameter("controller.stanley_ke", 0.1)
        self.declare_parameter("controller.stanley_ke", 0.1)
        self._stanley_ke = 0.0
        # self.sub_thread = Thread(target=self.param_thread, daemon=True)
        # self.sub_thread.start()

    # def param_thread(self):
    #     self.timer = self.node.create_timer(
    #         1,
    #         self.timer_callback,
    #     )
    #     rclpy.spin(self.node)

    # def timer_callback(self):
    #     my_param = (
    #         self.node.get_parameter("controller.stanley_ke")
    #         .get_parameter_value()
    #         .string_value
    #     )
    #     self.node.get_logger().info("Hello %s!" % my_param)

    #     my_new_param = rclpy.parameter.Parameter(
    #         "controller.stankley_ke",
    #         rclpy.Parameter.Type.DOUBLE,
    #         self._stanley_ke,
    #     )
    #     all_new_parameters = [my_new_param]
    #     self.node.set_parameters(all_new_parameters)

    def new_timer_callback(self):
        my_param = (
            self.get_parameter("controller.stanley_ke")
            .get_parameter_value()
            .double_value
        )
        self.get_logger().info("Hello %s!" % my_param)

        my_new_param = rclpy.parameter.Parameter(
            "controller.stanley_ke",
            rclpy.Parameter.Type.DOUBLE,
            self._stanley_ke,
        )
        all_new_parameters = [my_new_param]
        self.set_parameters(all_new_parameters)

    def set_stanley_ke(self, stanley_ke_val: float):
        self._stanley_ke = stanley_ke_val

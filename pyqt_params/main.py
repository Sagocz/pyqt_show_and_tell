#!/usr/bin/env python3
import sys
import rclpy
import argparse
from PyQt5 import QtWidgets, QtGui

from controller import Controller


class Window(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Controller()
        # self.setWindowIcon(QtGui.QIcon(str(WINDOW_ICON_PATH)))

    def showAndSetup(self):
        self.ui.setupUi(self)
        self.show()


def main(argv):
    rclpy.init()
    app = QtWidgets.QApplication(argv)
    window = Window()
    window.showAndSetup()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main(sys.argv)

#!/usr/bin/env python
from rclpy.node import Node
from as_vehicle_bb1_msgs.msg import RawControlCommandOneAxis
from PyQt5.QtCore import QObject


class WidgetSending(Node, QObject):
    def __init__(self):
        super(WidgetSending, self).__init__("widget_publisher")

        self.control_cmd_publisher = self.create_publisher(
            RawControlCommandOneAxis, "/bb1_control_cmd", 10
        )

    def return_actual_order_state(self):
        return self.__actual_order_state

    def control_cmd(self, throttle, brake, steer_angle):
        msg = RawControlCommandOneAxis()
        msg.stamp = self.get_clock().now().to_msg()
        msg.throttle = throttle
        msg.brake = brake
        msg.steer_angle = steer_angle
        self.control_cmd_publisher.publish(msg)

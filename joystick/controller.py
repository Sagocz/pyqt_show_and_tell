#!/usr/bin/env python
from publisher import WidgetSending
from subscriber import WidgetReceiver
from widget_example import Ui_MainWindow


from PyQt5 import QtCore
from PyQt5.QtCore import QObject
from joypad import Joystick


class Controller(Ui_MainWindow, QObject):
    # Setup elements on GUI

    def setupUi(self, MainWindow):
        # Calling generated class setupUi
        Ui_MainWindow.setupUi(self, MainWindow)

        self.widget_subscriber_node = WidgetReceiver()
        self.widget_publisher_node = WidgetSending()
        self.joystick = Joystick(self.widget_publisher_node)
        self.gridLayout.addWidget(self.joystick, 0, 0)
        ### Visibility Setup ###
        self.frameJoypad.setDisabled(True)
        self.frameJoypad.setVisible(False)
        self.disable_control.setChecked(True)
        #### Radio Buttons Connecting ####
        self.enable_control.clicked.connect(self.enable_control_clicked)
        self.disable_control.clicked.connect(self.disable_control_clicked)

        self.widget_subscriber_node.widget_signal.connect(
            self.update_actual_order_state
        )

    @QtCore.pyqtSlot()
    def update_actual_order_state(self):
        order_actual_state = self.widget_subscriber_node.return_actual_order_state()
        self.order_label.setText(order_actual_state)

    ### Control choice methods ###
    @QtCore.pyqtSlot()
    def disable_control_clicked(self):
        self.frameJoypad.setDisabled(True)
        self.frameJoypad.setVisible(False)

    @QtCore.pyqtSlot()
    def enable_control_clicked(self):
        self.frameJoypad.setEnabled(True)
        self.frameJoypad.setVisible(True)

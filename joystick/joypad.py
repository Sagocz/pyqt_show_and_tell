#!/usr/bin/env python
import math

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt

from publisher import WidgetSending


class Joystick(QtWidgets.QWidget):
    def __init__(self, publisher: WidgetSending, parent=None):
        super(Joystick, self).__init__(parent)
        self.publisher_node = publisher
        self.setMinimumSize(100, 100)
        self.moving_offset = QtCore.QPointF(0, 0)
        self.grab_center = False
        self.__max_distance = 50
        self.circle_center = QtCore.QPointF(0, 0)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        bounds = QtCore.QRectF(
            -self.__max_distance,
            -self.__max_distance,
            self.__max_distance * 2,
            self.__max_distance * 2,
        ).translated(self.__center())
        painter.drawEllipse(bounds)
        painter.setBrush(Qt.black)
        painter.drawEllipse(self.__center_ellipse())

    def __center_ellipse(self):
        if self.grab_center:
            return QtCore.QRectF(-20, -20, 40, 40).translated(
                self.moving_offset,
            )
        return QtCore.QRectF(-20, -20, 40, 40).translated(self.__center())

    def __center(self):
        return QtCore.QPointF(self.width() / 2, self.height() / 2)

    def _boundJoystick(self, point):
        limit_line = QtCore.QLineF(self.__center(), point)
        if limit_line.length() > self.__max_distance:
            limit_line.setLength(self.__max_distance)
        return limit_line.p2()

    @QtCore.pyqtSlot()
    def mousePressEvent(self, ev):
        self.grab_center = self.__center_ellipse().contains(ev.pos())
        return super(Joystick, self).mousePressEvent(ev)

    @QtCore.pyqtSlot()
    def mouseReleaseEvent(self, event):
        self.grab_center = False
        self.moving_offset = QtCore.QPointF(0, 0)
        self.update()
        self.publisher_node.control_cmd(
            throttle=0.0,
            brake=0.0,
            steer_angle=0.0,
        )

    @QtCore.pyqtSlot()
    def mouseMoveEvent(self, event):
        widget_max_y = 40
        widget_min_y = 140
        widget_max_x = 165
        widget_min_x = 65
        throttle_max = 1.0
        throttle_min = 0.0

        steering_max = math.radians(15.0)
        steering_min = math.radians(-15.0)
        widget_y_center = 90

        moving_offset_x = 0
        moving_offset_y = 0
        if self.grab_center:
            self.moving_offset = self._boundJoystick(event.pos())

            self.update()
            moving_offset_y = int(self.moving_offset.y())
            moving_offset_x = int(self.moving_offset.x())

        left_span_x = widget_max_x - widget_min_x
        steering_span = steering_max - steering_min

        scaled_value_x = float(moving_offset_x - widget_min_x) / float(left_span_x)
        steering_angle = float(steering_min + (scaled_value_x * steering_span))

        throttle_span = throttle_max - throttle_min
        if moving_offset_y >= widget_y_center:
            throttle = 0.0
            left_span_y = widget_min_y - widget_y_center
            valueScaledY = float(moving_offset_y - widget_y_center) / float(left_span_y)
            brake = float(throttle_min + (valueScaledY * throttle_span))

        else:
            brake = 0.0
            left_span_y = widget_max_y - widget_y_center
            valueScaledY = float(moving_offset_y - widget_y_center) / float(left_span_y)
            throttle = float(throttle_min + (valueScaledY * throttle_span))

        self.publisher_node.control_cmd(
            throttle=throttle,
            brake=brake,
            steer_angle=steering_angle,
        )

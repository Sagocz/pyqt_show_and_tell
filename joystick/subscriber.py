#!/usr/bin/env python
import rclpy
from std_msgs.msg import String
from PyQt5.QtCore import QObject, pyqtSignal
from threading import Thread


class WidgetReceiver(QObject):
    widget_signal = pyqtSignal()

    def __init__(self):
        super(WidgetReceiver, self).__init__()
        QObject.__init__(self)
        self.__actual_order_state = "N/A"
        self.node = rclpy.create_node("widget_subscriber")

        self.sub_thread = Thread(target=self.some_thread, daemon=True)
        self.sub_thread.start()

    def some_thread(self):
        self.subscription = self.node.create_subscription(
            String, "/jedzenie/kebab", self.order_callback, 10
        )
        rclpy.spin(self.node)

    def order_callback(self, msg):
        self.__actual_order_state = msg.data
        self.widget_signal.emit()

    def return_actual_order_state(self):
        return self.__actual_order_state

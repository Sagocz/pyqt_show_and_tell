#!/usr/bin/env python
import rclpy
from PyQt5.QtCore import QObject, pyqtSignal
from threading import Thread
from as_vehicle_bb1_msgs.msg import RawControlCommandOneAxis
from std_msgs.msg import Float32, Bool


class DisplayerMockSubscriber(QObject):
    displayer_mock_signal = pyqtSignal()

    def __init__(self):
        super(DisplayerMockSubscriber, self).__init__()
        QObject.__init__(self)
        self.__ctrl_cmd_msg_values = RawControlCommandOneAxis()
        self.__deceleration_value = 0.0
        self.__flag_value = None

        self.node = rclpy.create_node("displayer_mock_subscriber")

        self.sub_thread = Thread(target=self.subscriber_thread, daemon=True)
        self.sub_thread.start()

    def subscriber_thread(self):
        self.subscription_one = self.node.create_subscription(
            RawControlCommandOneAxis,
            "/bb1_control_cmd",
            self.raw_ctrl_cmd_callback,
            10,
        )
        self.subscription_two = self.node.create_subscription(
            Float32,
            "/deceleration",
            self.deceleration_callback,
            10,
        )
        self.subscription_three = self.node.create_subscription(
            Bool,
            "/bool/flag",
            self.flag_callback,
            10,
        )
        rclpy.spin(self.node)

    def raw_ctrl_cmd_callback(self, msg):
        self.__ctrl_cmd_msg_values.stamp = msg.stamp
        self.__ctrl_cmd_msg_values.throttle = msg.throttle
        self.__ctrl_cmd_msg_values.brake = msg.brake
        self.__ctrl_cmd_msg_values.steer_angle = msg.steer_angle
        self.displayer_mock_signal.emit()

    def deceleration_callback(self, msg):
        self.__deceleration_value = msg.data
        self.displayer_mock_signal.emit()

    def flag_callback(self, msg):
        self.__flag_value = msg.data
        self.displayer_mock_signal.emit()

    def return_ctrl_cmd_msg_values(self):
        return self.__ctrl_cmd_msg_values

    def return_deceleration_msg_value(self):
        return self.__deceleration_value

    def return_flag_msg_value(self):
        return self.__flag_value

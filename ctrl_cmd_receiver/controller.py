#!/usr/bin/env python
from datetime import datetime
from pathlib import Path

from PyQt5 import QtCore
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtGui import QPixmap

from subscriber import DisplayerMockSubscriber
from displayer import Ui_MainWindow

GRAPHIC_PATH = Path(__file__).parent / "img"


class Controller(Ui_MainWindow, QObject):
    # Setup elements on GUI

    def setupUi(self, MainWindow):
        # Calling generated class setupUi
        Ui_MainWindow.setupUi(self, MainWindow)

        self.widget_subscriber_node = DisplayerMockSubscriber()
        self.marek_pixmap = QPixmap(str(GRAPHIC_PATH / "marek.jpg"))
        self.hubert_pixmap = QPixmap(str(GRAPHIC_PATH / "hubert.jpg"))

        self.widget_subscriber_node.displayer_mock_signal.connect(
            self.update_ctrl_cmd_display
        )
        self.widget_subscriber_node.displayer_mock_signal.connect(
            self.update_deceleration_display
        )
        self.widget_subscriber_node.displayer_mock_signal.connect(
            self.update_pixmap_label
        )
        self.deceleration_box = QMessageBox()

    @QtCore.pyqtSlot()
    def update_ctrl_cmd_display(self):
        ctrl_cmd_values = self.widget_subscriber_node.return_ctrl_cmd_msg_values()
        msg_sec = ctrl_cmd_values.stamp.sec
        msg_nsec = ctrl_cmd_values.stamp.nanosec
        msg_time = msg_sec + (msg_nsec * 1e-9)
        formated_time = datetime.fromtimestamp(msg_time).strftime(
            "%A, %B %d,\n %Y %I:%M:%S.%f"
        )
        self.throttle_display.display(ctrl_cmd_values.throttle)
        self.brake_display.display(ctrl_cmd_values.brake)
        self.steer_angle_display.display(ctrl_cmd_values.steer_angle)
        self.time_label.setText(str(formated_time))

    @QtCore.pyqtSlot()
    def update_deceleration_display(self):
        deceleration_value = self.widget_subscriber_node.return_deceleration_msg_value()
        self.deceleration_display.display(deceleration_value)

        if deceleration_value >= 6.0:
            self.deceleration_box_msg()

    @QtCore.pyqtSlot()
    def deceleration_box_msg(self):
        self.deceleration_box.setWindowTitle("WARNING!!!!")
        self.deceleration_box.setText("DECELERATION VALUE\nEXCEEDED!!!!!")
        self.deceleration_box.exec_()

    @QtCore.pyqtSlot()
    def update_pixmap_label(self):
        flag_value = self.widget_subscriber_node.return_flag_msg_value()

        if flag_value:
            self.flag_label.setPixmap(self.marek_pixmap)
        else:
            self.flag_label.setPixmap(self.hubert_pixmap)

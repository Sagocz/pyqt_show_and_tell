#!/usr/bin/env python
from rclpy.node import Node
from std_msgs.msg import String, Bool, Float32
from PyQt5.QtCore import QObject


class ButtonsSending(Node, QObject):
    def __init__(self):
        super(ButtonsSending, self).__init__("buttons_publisher")

        self.kebab_cmd_publisher = self.create_publisher(
            String,
            "/jedzenie/kebab",
            10,
        )

        self.flag_cmd_publisher = self.create_publisher(
            Bool,
            "/bool/flag",
            10,
        )

        self.decel_publisher = self.create_publisher(
            Float32,
            "/deceleration",
            10,
        )

    def kebab_cmd(self, kebab_name: String):
        msg = String()
        msg.data = kebab_name
        self.kebab_cmd_publisher.publish(msg)

    def flag_cmd(self, flag: bool):
        msg = Bool()
        msg.data = flag
        self.flag_cmd_publisher.publish(msg)

    def decel_sender(self, deceleration: float):
        msg = Float32()
        msg.data = deceleration
        self.decel_publisher.publish(msg)

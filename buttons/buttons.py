# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'buttons/buttons.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(276, 310)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.push_button_one = QtWidgets.QPushButton(self.centralwidget)
        self.push_button_one.setGeometry(QtCore.QRect(10, 130, 89, 25))
        self.push_button_one.setObjectName("push_button_one")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 10, 151, 111))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.radio_one = QtWidgets.QRadioButton(self.frame)
        self.radio_one.setGeometry(QtCore.QRect(0, 10, 131, 23))
        self.radio_one.setObjectName("radio_one")
        self.radio_two = QtWidgets.QRadioButton(self.frame)
        self.radio_two.setGeometry(QtCore.QRect(0, 40, 131, 23))
        self.radio_two.setObjectName("radio_two")
        self.radio_three = QtWidgets.QRadioButton(self.frame)
        self.radio_three.setGeometry(QtCore.QRect(0, 70, 131, 23))
        self.radio_three.setObjectName("radio_three")
        self.push_button_two = QtWidgets.QPushButton(self.centralwidget)
        self.push_button_two.setGeometry(QtCore.QRect(10, 160, 89, 25))
        self.push_button_two.setObjectName("push_button_two")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(40, 200, 191, 17))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.deceleration_slider = QtWidgets.QSlider(self.centralwidget)
        self.deceleration_slider.setGeometry(QtCore.QRect(19, 230, 241, 31))
        self.deceleration_slider.setMinimum(-80)
        self.deceleration_slider.setMaximum(80)
        self.deceleration_slider.setPageStep(10)
        self.deceleration_slider.setSliderPosition(0)
        self.deceleration_slider.setTracking(True)
        self.deceleration_slider.setOrientation(QtCore.Qt.Horizontal)
        self.deceleration_slider.setInvertedControls(False)
        self.deceleration_slider.setTickPosition(QtWidgets.QSlider.TicksBothSides)
        self.deceleration_slider.setTickInterval(0)
        self.deceleration_slider.setObjectName("deceleration_slider")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 276, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Buttons Mock"))
        self.push_button_one.setText(_translate("MainWindow", "Set True"))
        self.radio_one.setText(_translate("MainWindow", "RadioButton 1"))
        self.radio_two.setText(_translate("MainWindow", "RadioButton 2"))
        self.radio_three.setText(_translate("MainWindow", "RadioButton 3"))
        self.push_button_two.setText(_translate("MainWindow", "Set False"))
        self.label.setText(_translate("MainWindow", "DECELERATION SETTER"))


#!/usr/bin/env python3
import sys
import rclpy
from PyQt5 import QtWidgets

from controller import Controller


class Window(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Controller()

    def showAndSetup(self):
        self.ui.setupUi(self)
        self.show()


def main(argv):
    # ROS init
    rclpy.init()
    app = QtWidgets.QApplication(argv)
    window = Window()
    window.showAndSetup()

    # app infinite loop
    sys.exit(app.exec_())


if __name__ == "__main__":
    main(sys.argv)

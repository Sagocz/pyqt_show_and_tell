#!/usr/bin/env python
from PyQt5 import QtCore
from PyQt5.QtCore import QObject

from buttons import Ui_MainWindow
from publisher import ButtonsSending


class Controller(Ui_MainWindow, QObject):
    # setup elements on gui
    def setupUi(self, Form):
        Ui_MainWindow.setupUi(self, Form)
        self.publisher = ButtonsSending()
        self.radio_one.setChecked(True)
        self.radio_one.clicked.connect(self.send_kebab_msg_one)
        self.radio_two.clicked.connect(self.send_kebab_msg_two)
        self.radio_three.clicked.connect(self.send_kebab_msg_three)
        self.push_button_one.clicked.connect(self.send_flag_true)
        self.push_button_two.clicked.connect(self.send_flag_false)
        self.deceleration_slider.valueChanged.connect(self.send_deceleration_msg)

    @QtCore.pyqtSlot()
    def send_kebab_msg_one(self):
        self.publisher.kebab_cmd("kebab rollo")

    @QtCore.pyqtSlot()
    def send_kebab_msg_two(self):
        self.publisher.kebab_cmd("kebab pita")

    @QtCore.pyqtSlot()
    def send_kebab_msg_three(self):
        self.publisher.kebab_cmd("kebab w bułce")

    @QtCore.pyqtSlot()
    def send_flag_true(self):
        self.publisher.flag_cmd(True)

    @QtCore.pyqtSlot()
    def send_flag_false(self):
        self.publisher.flag_cmd(False)

    @QtCore.pyqtSlot()
    def send_deceleration_msg(self):
        decel_val = self.deceleration_slider.value() / 10

        self.publisher.decel_sender(deceleration=decel_val)
